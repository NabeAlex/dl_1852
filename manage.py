from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import flask.ext.whooshalchemy as whooshalchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db'

db = SQLAlchemy(app)

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    
    ### Unique user id ###
    uid = db.Column(db.String(128), unique = True)
    ######################

    ### Password's user ###
    password = db.Column(db.String(128))
    #######################

    name = db.Column(db.String(128), nullable = False)
    sur_name = db.Column(db.String(128), nullable = False)

    years = db.Column(db.String(128), nullable = False)

    # Addition #
    email = db.Column(db.String(128))
    phone = db.Column(db.String(128))
    ############

    class_ = db.Column(db.String(128), nullable = False)
    number = db.Column(db.String(128), nullable = False)

    # ROLE #
    # """  #
    # """  #	"admin", "librarian", "pupil", "teacher"
    # """  #
    role = db.Column(db.String(128))
    role_title = db.Column(db.String(128))
    ########

    ### HASH ###
    scan = db.Column(db.String(128))
    ############


    def __repr__(self):
    	message = ("<User " + (self.sur_name) + ">")
        return message


    def isPassword(self, password_ = ''):
        if(password_ == self.password):
            return True
        return False

class Book(db.Model):
    __searchable__ = ['sur_author']

    id = db.Column(db.Integer, primary_key = True)
	
	### COUNT ###
    count_books = db.Column(db.Integer)
	#############

    name_author = db.Column(db.String(128), nullable = False)
    sur_author = db.Column(db.String(128), nullable = False)
    title = db.Column(db.String(256), nullable = False)

	# Addition #
    middle_author = db.Column(db.String(128))
    publisher_company = db.Column(db.String(256))
    years = db.Column(db.Integer)
    category = db.Column(db.String(128))
    count_pages = db.Column(db.Integer)

    description = db.Column(db.Text)
    full_description = db.Column(db.Text)
	############

    # new
    mark = db.Column(db.String(128))

    ### HASH ###
    scan = db.Column(db.String(128))
    ############

    def __repr__(self):
        message = ("<Book " + (self.title) + ">")
        return message

whooshalchemy.whoosh_index(app, Book)

class BooksForPupil(db.Model):
    id = db.Column(db.INTEGER, primary_key = True, autoincrement = True)
    
    pupil_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable = True)
    book_id = db.Column(db.Integer, db.ForeignKey('book.id'), nullable = True)
    
    time_pass  = db.Column(db.Integer, nullable = False)
    time = db.Column(db.TIMESTAMP, nullable = False)

    p_id = db.relationship(User, foreign_keys=[pupil_id], backref = 'sent')
    b_id = db.relationship(Book, foreign_keys=[book_id], backref = 'received')

    # hand, wait
    status = db.Column(db.String(128))

    # COMMENT #
    comment = db.Column(db.Text)
    ###########
    
class Message(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    
    from_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable = False)

    ### MESSAGE ###
    topic = db.Column(db.String(256))
    text = db.Column(db.Text)
    ###############

    time = db.Column(db.TIMESTAMP, nullable = False)

    from_pupil = db.relationship(User, foreign_keys = [from_id], backref = 'message')

class News(db.Model):
    id = db.Column(db.Integer, primary_key = True)

    topic = db.Column(db.Text)
    main = db.Column(db.Text)
    time = db.Column(db.TIMESTAMP)

class Blog(db.Model):
    id = db.Column(db.Integer, primary_key = True)

    topic = db.Column(db.Text)

    sub = db.Column(db.Text)
    main = db.Column(db.Text)

    time = db.Column(db.TIMESTAMP)

class FeedBack(db.Model):
    id = db.Column(db.Integer, primary_key = True)

    text = db.Column(db.Text)
    
    time = db.Column(db.TIMESTAMP, nullable = False)

if __name__ == '__main__':
    manager.run()