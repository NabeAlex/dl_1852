from models import sessionUser, getMode

from manage import User, Book, BooksForPupil

from flask import render_template

def getMyBooks():
	user = sessionUser(min = 1, max = 2)
	all_ = BooksForPupil.query.filter_by(p_id = user)
	if(all_.count() == 0):
		return [None]
	return BooksForPupil.query.filter_by(p_id = user) 

def getMyTeacher():
	user = sessionUser(min = 1, max = 2)
	teacher = User.query.filter_by(class_ = user.class_, number = user.number, role = "teacher").first()
	
	if(user.class_ == ''):
		return render_template("include/block_error.html", error = "Error: NO CLASS_ IN user")

	if(teacher):
		return render_template("include/block_teacher.html", user = teacher)
	return "<h1 style='margin:30px;'>" +                                               \
		   u"\u041f\u043e\u043a\u0430 \u0443\u0447" +       						   \
		   u"\u0438\u0442\u0435\u043b\u044f \u043d\u0435\u0442 " + 					   \
		   u"\u043d\u0430 \u0441\u0430\u0439\u0442\u0435" +       					   \
		   "</h1>"

def getMyPupils():
	user = sessionUser(min = 1, max = 2)
	if(user):
		pupils = User.query.filter_by(class_ = user.class_, number = user.number, role = "pupil")
		tmp = []
		n = 1
		for i in pupils:
			tmp.append([n, i])
			n += 1

		return tmp 

	return []


def normalizeStatus(args):
	if(args[0] == "wait"):
		return u"\u0416\u0434\u0435\u0442"
	elif(args[0] == "hand"):
		return u"\u0412\u0437\u044f\u0442\u0430"
	else:
		return u"\u041d\u0435\u0438\u0437\u0432\u0435\u0441\u0442\u043d\u043e"

# With out WHO
menu = [	  {
				"text": [u"\u041e\u0441\u043d\u043e\u0432\u043d\u043e\u0435",
						 u"\u0417\u0430\u0447\u0435\u043c?",
						 u"\u041d\u0430\u0447\u043d\u0435\u043c"],
				"link": ['/welcome', '/why', '/begin']
			  }, 
			  {
				"text": [u"\u041c\u043e\u0438 \u043a\u043d\u0438\u0433\u0438",
						 u"\u041c\u043e\u0439 \u0443\u0447\u0438\u0442\u0435\u043b\u044c",
						 u"\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435"],
				"link": ["/mybook", "/myteacher", "/message"]
			  },
			  {
			  	"text": [u"\u041a\u043d\u0438\u0433\u0438 \u0432\u0430\u0448\u0438\u0445 \u0443\u0447\u0435\u043d\u0438\u043a\u043e\u0432",
			  			 u"\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435"],
			  	"link": ["/mypupils", '/message']
			  },
			  {
			  	"text": [u"\u0420\u0430\u0431\u043e\u0447\u0435\u0435 \u043c\u0435\u0441\u0442\u043e",
			  			 u"\u0412\u0441\u0435 \u0443\u0447\u0435\u043d\u0438\u043a\u0438",
			  			 u"\u0414\u043e\u043b\u0436\u043d\u0438\u043a\u0438",
			  			 u"\u0412\u043e\u043f\u0440\u043e\u0441\u044b"],
			  	"link": ["/workplace", "/allpupils", "/debtors", "/questions"]
			  }
		]

def getMenu(args):
	global menu

	# BUG

	try:
		mode = getMode(sessionUser().role)
	except Exception, e:
		mode = 0

	curent = args[0]
	
	left = "<div class='sub-menu'><ul>"
	right = "</ul></div>"

	lis = menu[mode]

	str_ = ""
	tmp = ""
	for i in range(len(lis["text"])):
		if(i == curent):
			tmp = "style='background-color: #DDD;'"
		else:
			tmp = ""

		str_  += "<a href='" + lis["link"][i] + "'><li " + tmp + ">" + lis["text"][i] + "</li></a>"
	

	return left + str_ + right