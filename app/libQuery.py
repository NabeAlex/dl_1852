from manage import User, Book, BooksForPupil, Message
from flask import request

def getNowClass():
	c = str(request.args.get("c", ""))
	if(c == ""):
		c = "8"

	s = '<select class="input-select" name="c">'
	for i in range(1, 12):
		s += "<option " + ("selected='selected'" if c == str(i) else "") + " value='" + str(i) + "'>" + str(i) + "</option>"

	s += "</select>"
	return s

def getNowNumber():
	n = request.args.get("n", "")
	if(n == ""):
		n = u"\u0410"

	s = '<select class="input-select" name="n">'
	ar = [u"\u0410", u"\u0411", u"\u0412", u"\u0413", u"\u0414"]
	for i in ar:
		s += "<option " + ("selected='selected'" if n == i else "") + " value='" + i + "'>" + i + "</option>"

	s += "</select>"
	return s

def getAllPupils():
	c = request.args.get("c", "")
	n = request.args.get("n", "")
	if(c == ""):
		c = "8"
	if(n == ""):
		n = u"\u0410"

	pupils = User.query.filter_by(class_ = c, number = n, role = "pupil").all()
	if(pupils):
		tmp = []
		n = 1
		for i in pupils:
			tmp.append([n, i])
			n += 1

		if(len(tmp) == 0):
			tmp = [None]

		return tmp 

	return [None]

def getMessage():
	message = Message.query.all()
	if(message):
		return message

	return [None]

from datetime import date, timedelta, datetime

def getDebtors():
	debtors = BooksForPupil.query.all()
	if(debtors == None):
		return [None]

	vector = []
	for i in debtors:
		if(i.time + timedelta(days = i.time_pass) < datetime.now()):
			vector.append(i)

	if(len(vector) == 0):
		return [None]

	return vector

def getReservations():
	reser = BooksForPupil.query.filter_by(status = "wait").all()
	l = ["", ""]
	n = 0
	for i in reser:
		index = n % 2
		l[index] += "<tr>"
		l[index] += "<td>" + i.p_id.sur_name + " " + i.p_id.name + " <b>" + i.p_id.class_ + ' "' + i.p_id.number + "\"</b>" + "</td>"
		l[index] += "<td>" + i.comment + "</td>"
		l[index] += "<td><a href='/user/" + i.p_id.scan + "'>" + i.p_id.scan + "</a></td>"
		l[index] += "</tr>"
		n += 1

	su = u'''
		<div class="half-topic">
			<table>
				<tr style="background: #EAEFFF;">
					<td>\u0424\u0418\u041a</td>
					<td>\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439</td>
					<td>\u041f\u043e\u0434\u0440\u043e\u0431\u043d\u0435\u0435</td>
				</tr>
				|
			</table>
		</div>
		'''
	if(len(l[0]) == 0):
		return ""
	if(len(l[1]) == 0):
		return su.replace("|", l[0])

	return su.replace("|", l[0]) + su.replace("|", l[1])