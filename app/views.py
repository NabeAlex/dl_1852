from flask import render_template, request, session, redirect, url_for, jsonify
from app import app
from forms import *

from manage import db, User, Book, BooksForPupil, News, Message, FeedBack

from utils import *
from models import *
from mainQuery import get

import os

from datetime import datetime

@app.route('/')
def empty():
	return index("")

@app.route('/<path>')
def index(path):
	if(path in redirectMap):
		path = redirectMap[path]

	user = None
	mode = 0
	if("id" in session):
		user = checkUser(session["id"])
		if(user):
			mode = getMode(user.role)
		else:
			user = None
			session.clear()
			path = "welcome"

	if(path in hashMap):
		tmp = hashMap[path]
		# FOR WELCOME
		if(mode > 0 and (tmp.mode == 0 and not tmp.high)):
			path = "cabinet"
			return redirect(url_for('index', path = path))

		if((tmp.mode < mode and tmp.high) or (tmp.mode == mode)):
			# SUCCESS
			return getTemplate(toPath(path), user)
		else:
			return getTemplate(toPath("notavaible"), user)
	else:
		return getTemplate(toPath("404"))
	
	return getTemplate(toPath("welcome"), user)

@app.route('/signup', methods = ['GET', 'POST'])
def signup():
	type = request.args.get('t', '')
	isPOST = (request.method == 'POST')

	result = "Error"
	if(type == "teacher"):
		result = login(request.form, type) if (isPOST) else authField(type)
	elif(type == "librarian"):
		pass
	else:
		result = login(request.form, type) if (isPOST) else authField(type)
	return result

@app.route('/auth')
def auth():
	q = request.args.get('q', '')
	return render_template(toPath("form"), title = "Authorization", content = "auth.html", message = q)

@app.route('/user/<username>')
def profile(username): 
	mode = 0
	if("role" in session):
		mode = getMode(session["role"])
		if(not checkPerm("user", mode)):
			return username
	else:
		return username

	if(username == "all"):
		return render_template("index.html", title = "Digital library | ALL", 			\
			includes_blocks = [], get = get,										    \
			content = "user.html", one = False, 
			users = User.query.filter((User.password == '') | 
									  (User.password == None)).all())
	else:
		user = User.query.filter_by(scan = username).first()
		b = BooksForPupil.query.filter_by(p_id = user)
		bs = []
		b_reserv = []

		for i in b:
			if(i.status == "hand"):
				delta_time = deltaDays(i.time, i.time_pass)
				bs.append([i.id, i.b_id, delta_time])
			elif(i.status == "wait"):
				b_reserv.append([i.id, i.b_id, i.time.strftime("%d-%m-%Y"), i.comment])

		if(len(bs) == 0):
			bs = [None]
		if(len(b_reserv) == 0):
			b_reserv = [None]

		if(user):
			return render_template("index.html", title = "Digital library | " + username, 			\
					includes_blocks = generateLeftBlocks(mode = mode), get = get,					\
					content = "user.html", one = True, user = getCurrentUser(), 					\
													   books = bs, book_reserv = b_reserv, mode = mode, user_ = user)

	return username

def authField(type):
	return render_template(toPath("form"), title = "Digital library | Sign up", content = "signup.html", who = type, form = LoginForm())

def newsField():
	return render_template(toPath("form"), title = "Digital library | Add news", content = "newnews.html", form = NewsForm())
# BOOK #


@app.route('/books/')
@app.route('/books/<page>')
def book(page = ""):
	if(page == ""):
		page = 1

	try:
		page = int(page)
	except Exception, e:
		page = 1

	user = getCurrentUser()

	return render_template("index.html", title = "Digital library",       \
				includes_blocks = generateLeftBlocks(user), user = user,  \
				get = get, book_page = page, content = "books.html")

@app.route('/book/')
@app.route('/book/<book>')
def book_profile(book = ""):

	if("role" in session and "id" in session):
		try:
			user = getCurrentUser(session["id"])
			mode = getMode(session["role"])
		except Exception, e:
			session.clear()
			return getTemplate(toPath("notavaible"), None)
	else:
		user = None
		mode = 0

	someBook = Book.query.filter_by(scan = book).first()

	if(not someBook):
		return getTemplate(toPath("404"), user)
	
	ex_path = '/static/image/books/' + genScan(someBook.id, False, 6) + '.jpg'
	
	file = {"is": False}
	if(os.path.isfile(os.path.dirname(__file__) + ex_path)):
		file = {"is": True, "path": ex_path}
	
	if(not someBook):
		return getTemplate(toPath("404"))

	
	return getTemplate(toPath("book"), user, [toPath("block_book_image")], [someBook, file])


####################
#                  #
#	POST Methods   #
#                  #
####################


def login(f, who):
	name, surName, D, M, Y, class_, number,  \
	email, phone  =				             \
		    f.get('name', ''),               \
		 	f.get('last_name', ''),          \
		 	f.get('date_D', ''),					 \
		 	f.get('date_M', ''),					 \
		 	f.get('date_Y', ''),                  \
		 	f.get('select_class', ''),       \
		 	f.get('number_class', ''),       \
		 	f.get('email', ''),              \
			f.get('phone', '')

	role_title = f.get('role', '')
	date = [D, M, Y]

	uid = generateUID(name, surName, date)
	
	newUser = User(
		uid = uid,
		name = name,
		sur_name = surName,
		years = '-'.join(date),
		class_ = class_,
		number = number,
		email = email,
		phone = phone,
		role = who,
		role_title = role_title
	)

	db.session.add(newUser)

	try:
	    db.session.commit()
	    newUser.scan = genScan(newUser.id)
	    db.session.commit()
	except:
		db.session.rollback()

	return redirect(url_for('index', path = "", e = "ok"))

@app.route('/login', methods = ["POST"])
def inServer():
	login = request.form.get('login', '')
	password = request.form.get('password', '')

	user = User.query.filter(User.uid == login);

	if(0 < user.count() <= 1 and len(password) > 2):
		if(User.isPassword(user[0], password)):
			session["id"] = user[0].id
			session["role"] = user[0].role
			return redirect(url_for('index', path = "cabinet"))
	elif(user.count() > 1):
		return "Call teacher!"

	return redirect(url_for('auth', q = "Wrong login or password"))

@app.route('/loginout', methods = ["POST"])
def outServer():
	session.clear()
	return redirect(url_for('index', path = "welcome"))

# LOGIC #

def getTemplate(component, user = None, lefts = [], args = []):
	if(user == None):
		return render_template("index.html", title = "Digital library", \
			includes_blocks = lefts + generateLeftBlocks(),   \
			content = component, get = get, mode = 0, args = args)

	return render_template("index.html", title = "Digital library", \
		user = user, \
		includes_blocks = lefts + generateLeftBlocks(user),   \
		content = component, get = get, mode = getMode(user.role), args = args)	


########	
# AJAX #
########

@app.route("/ajax/user")
def ajaxUser():
	scan = getScan(request.args)

	if(scan != ''):
		user = User.query.filter_by(scan = scan).first()
		if(user):
			return jsonify({"status": "OK"})
	return jsonify(error())

@app.route("/ajax/book")
def ajaxBook():
	scan = getScan(request.args)

	if(scan != ''):
		book = Book.query.filter_by(scan = scan).first()
		if(book):
			return jsonify({"status": "OK", "data": {"id": book.id, "name": book.sur_author, "title": book.title}})
	return jsonify(error())

@app.route("/ajax/push")
def pushBook():
	scan = request.args.get('to', '')
	
	try:
		user = User.query.filter_by(scan = scan).first()
	except Exception, e:
		return jsonify(error())
	
	try:
		n = int(request.args.get('n', ''))
		id_user = int(request.args.get('to', ''))
	except Exception, e:
		return jsonify(error())

	if(n < 10):
		for i in range(n):
			tmp = request.args.get("b" + str(i), '')
			if(tmp != '' and isInt(tmp)):
				book = getBook(int(tmp))
				if(book):
					t = getTime()
					newPass = BooksForPupil(p_id = user, b_id = book, time_pass = 14, time = t, status = "hand")
					db.session.add(newPass)

					if(book.count_books > 0):
						book.count_books -= 1;
						
				else:
					break
			else:
				break
		else:
			db.session.commit()
			return jsonify({"status": "OK"})
		return jsonify(error())

@app.route('/ajax/setbook')
def setBook():
	scan = getScan(request.args)
	try:
		days = int(request.args.get('days', ''))
	except Exception, e:
		return jsonify(error())
	
	if(scan != ''):
		book = Book.query.filter_by(scan = scan).first()
		if(book):
			book.time_pass = days
			db.session.commit()
			return jsonify({"status": "OK"})

	return jsonify(error())

@app.route('/ajax/tobook')
def toBook():
	id_ = request.args.get("id", '')
	if(isInt(id_)):
		book_pupil = BooksForPupil.query.filter_by(id = id_).first()
		if(book_pupil):
			if(book_pupil.b_id.count_books > 0):
				book_pupil.b_id.count_books -= 1;

			book_pupil.status = "hand"
			book_pupil.time = getTime()
			
			db.session.commit()

			return jsonify({"status": "OK"})
	
	return jsonify(error())

@app.route('/ajax/setwait')
def setWait():
	action = request.args.get("act", "on")
	if(action == "on"):
		id_ = request.args.get("book", '')
		if(isInt(id_)):
			book = getBook(int(id_))
			if(book):
				user = sessionUser(max = 3)
				
				also = BooksForPupil.query.filter_by(status = "wait", p_id = user, b_id = book).first()
				if(also):
					return jsonify({"status": "Again"})

				# For teacher!!!
				
				if(user):
					t = getTime()
					
					newPass = BooksForPupil(p_id = user, 
										    b_id = book, 
										    time_pass = 14,
										    time = t, 
										    status = "wait",
										    comment = request.args.get("comment"))
					
					db.session.add(newPass)
					db.session.commit()
					return jsonify({"status": "OK"})
					
	return jsonify(error())

@app.route('/ajax/delbook')
def delBook():
	id_ = request.args.get('id', '')
	book_pupil = BooksForPupil.query.filter_by(id = id_).first()
	if(book_pupil):
		if(book_pupil.status != "wait"):
			book_pupil.b_id.count_books += 1

		db.session.delete(book_pupil)
		db.session.commit()
		return jsonify({"status": "OK"})

	return jsonify(error())

@app.route('/ajax/setmark')
def setMark():
	id_book =  request.args.get("id_book", "")
	mark = request.args.get("mark", "")
	if(id_book == "" or mark == ""):
		return jsonify(error())

	book = Book.query.filter_by(id = id_book).first()

	try:
		if(book):
			book.mark = mark
			db.session.commit()
			return jsonify({"status": "OK"})

	except Exception, e:
		pass

	return jsonify(error())

@app.route('/ajax/setdays')
def setDays(up = False):
	id_ = request.args.get("id", "")
	days = request.args.get("days", "")
	if(id_ == "" or days == ""):
		return jsonify(error())

	try:
		forPupil = BooksForPupil.query.filter_by(id = int(id_)).first()
		if(forPupil):
			if(not up):
				forPupil.time = getTime()
				forPupil.time_pass = int(days)
			else:
				forPupil.time_pass += int(days) 
			db.session.commit()
			return jsonify({"status": "OK", "data": {"id": id_, "new": deltaDays(forPupil.time, forPupil.time_pass)}})

	except Exception, e:
		pass

	return jsonify(error())

@app.route('/ajax/appenddays')
def appendDays():
	return setDays(True)

@app.route('/ajax/sendmessage', methods = ["POST"])
def sendMessage():
	from_id = request.form.get("from", "")
	topic = request.form.get("topic", "")
	message = request.form.get("message", "")
	
	if(from_id == ""):
		return redirect(url_for('index', path = "message", e = "no_auth"))
	if(topic.replace(" ", "") == ""):
		return redirect(url_for('index', path = "message", e = "no_topic"))
	
	if(message.replace(" ", "") == ""):
		return redirect(url_for('index', path = "message", e = "no_message"))

	m = Message(from_id = from_id, topic = topic, text = message, time = getTime())
	db.session.add(m)

	db.session.commit()

	return redirect(url_for('index', path = "message", e = "ok"))

# PUBLIC #

@app.route('/ajax/feedback', methods = ["POST"])
def feedBack():
	message = request.form.get("message", "")
	
	if(message.replace(" ", "") == ""):
		return redirect(url_for('index', path = "welcome", e = "no_message"))

	fb = FeedBack(text = message, time = getTime())
	db.session.add(fb)
	db.session.commit()

	return redirect(url_for('index', path = "welcome", e = "ok"))

########

# ADMIN #


@app.route('/admin/newnews', methods = ['GET', 'POST'])
def newNews():
	isPOST = (request.method == 'POST')
	if(isPOST):
		title = request.form.get('title', '')
		text = request.form.get('text', '')

		date = datetime.now()

		news = News(topic = title, main = text, time = date)
		db.session.add(news)
		db.session.commit()

		return redirect(url_for('index', path = 'news'))
	
	else:
	
		return newsField()

@app.route('/admin/generate_password', methods = ['GET', 'POST'])
def generatePassword():
	user = sessionUser(min = 3, max = 4)
	if(user):
		s = request.form.get('id', '')
		if(s == ''):
			return getTemplate(toPath("404"), user)

		u = User.query.filter_by(id = s).first()
		if(u and (u.password == None or u.password == '')):
			print(generate_temp_password(5))
			u.password = generate_temp_password(5)
			db.session.add(u)
			db.session.commit()

			return render_template(toPath("form"), 
								   title = "Generate password", 
								   content = "secret_password.html", secret = u)
		else:
			return "Password has already been exist <a href='/user/all'>Back</a>"

	return getTemplate(toPath("404"), user)

@app.route('/admin/delete_message', methods = ['GET'])
def deleteMessage():
	user = sessionUser(min = 3, max = 4)
	if(user):
		id_ = request.args.get("del", "")
		me = Message.query.filter_by(id = id_).first()
		if(me):
			db.session.delete(me)
			db.session.commit()

			return redirect(url_for('index', path="questions"))

	return getTemplate(toPath("404"), user)