from manage import User, Book
from flask  import session

def toPath(path):
	return path + ".html"

def getScan(typeRequest):
	return typeRequest.get("scan", '')

def error():
	return {"status": "Error"}
	
class Component:

	def __init__(self, mode, andHigh = True, pathTrue = ""):
		self.mode = mode
		self.high = andHigh 

# 0 - NEW_USER
# 1 - PUPIL
# 2 - TEACHER
# 3 - LIBRARIAN
# 4 - ADMIN

hashMap = {
	# NEW #
	"books"      : Component(0),
	"news"       : Component(0),
	"info"       : Component(0),
	"blog"       : Component(0),

	"welcome"    : Component(0, False),
	"why"        : Component(0, False),
	"who"        : Component(0, False),
	"begin"      : Component(0, False),
	"about"      : Component(0, False),

	# PUPIL #
	"message"    : Component(1),
	"letter"     : Component(1),
	"cabinet"    : Component(1),

	"mybook"     : Component(1, False),
	"myteacher"  : Component(1, False),

	# TEACHER #
	"user"      : Component(2),
	
	"mypupil"    : Component(2, False),
	# PUPIL SEE YOUR PUPLS

	# LIBRARIAN #
	"workplace"  : Component(3, False), 
	"allpupils"  : Component(3, False),
	"debtors"    : Component(3, False),
	"questions"  : Component(3, False) 
}

# REDIRECT #
redirectMap = {}

redirectMap[""] = "welcome"
redirectMap["mybook"] = "cabinet"
redirectMap["mypupils"] = "cabinet" 
redirectMap["workplace"] = "cabinet"

############

def getMode(s):
	if(s == "pupil"):
		return 1
	elif(s == "teacher"):
		return 2
	elif(s == "librarian"):
		return 3
	elif(s == "admin"):
		return 4
	else:
		return 0

def checkPerm(path, mode):
	perm = hashMap[path]
	if(perm.high and mode >= perm.mode):
		return True
	if(not perm.high and mode == perm.mode):
		return True
	return False 
# SCAN - 13

def generateUID(name, surName, date):
	new_date = '-'.join(date)
	return (name[0].upper() + surName[0].upper() + new_date)

def getCurrentUser(id_ = None):
	if(id_ == None):
		if("id" in session):
			return User.query.filter_by(id = session["id"]).first()
		else:
			return None

	return User.query.filter_by(id = id_).first()

def sessionUser(min = 1, max = 4):
	if("id" in session):
		if(getMode(session["role"]) >= min and getMode(session["role"]) <= max):
			return User.query.filter_by(id = session["id"]).first()
	return False

def getBook(id_):
	return Book.query.filter_by(id = id_).first()

def checkUser(id_):
	user = User.query.filter(User.id == id_)
	if(user.count() != 1):
		return False

	return user.first()

def generateLeftBlocks(user = '', mode = -1):
	if((type(user) == str or user == None) and mode == -1):
		return ["block_auth.html", "block_menu.html"]
	try:
		role = getMode(user.role)
	except Exception, e:
		pass
	
	if(mode != -1):
		role = mode

	if(role == 1):
		return ["block_info.html"]
	elif(role == 2):
		return ["block_info.html"]
	elif(role == 3):
		return ["block_info.html"]
	else:
		return ["block_auth.html", "block_menu.html"]

from os import urandom

chars = "0123456789"
def generate_temp_password(length):
	global chars
	return "".join([chars[ord(c) % len(chars)] for c in urandom(length)])