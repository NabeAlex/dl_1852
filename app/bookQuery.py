from manage import User, Book
from flask import request

def getQuery(args = False):
	q = request.args.get("q", "")
	if(q == ""):
		return ""
	else:
		tmp = ""
		if(args == 'True'):
			tmp = "?q="
		return tmp + q

def getPages():
	return int((getCountBooks()) / 10) + 1

last_count = -1
def getCountBooks():
	global last_count
	if(last_count < 0):
		return Book.query.count()
	else:
		return last_count

	return Book.query.count() 

def isSet(args, count = -1):
	if(count == -1):
		count = getCountBooks()

	try:
		if(args[0] <= 0):
			return -1

		n = 10
		el = (args[0] - 1) * n

		if(el > count):
			return -1

		return el

	except Exception, e:

		return 0

cache = [None, None]
def getBooks(args):
	global last_count, cache
	q = request.args.get("q", "")
	
	query = []

	n = 10
	if(q == ""):
		el = isSet(args)
		query = Book.query.offset(el).limit(n).all()
		last_count = -1
		cache = [None, None]
	else:
		el = (args[0] - 1) * n
		if(cache[0] == q):
			print el
			return cache[1][el:(el + n)]

		query = Book.query.whoosh_search(q).all()
		last_count = len(query)
		if(last_count == 0):
			return [False]

		# CACHE 
		cache = [q, query]
		return cache[1][el:(el + n)]

	return query

# 
static_mark = {5: u"\u041e\u0442\u043b\u0438\u0447\u043d\u043e\u0435",	      \
			   4: u"\u0425\u043e\u0440\u043e\u0448\u0435\u0435",		      \
			   3: u"\u0421\u0440\u0435\u0434\u043d\u0435\u0435",			  \
			   2: u"\u041f\u043b\u043e\u0445\u043e\u0435",                    \
			   1: u"\u0418\u0441\u043f\u043e\u0440\u0447\u0435\u043d\u0430",  \
			   0: u"\u0423\u0436\u0430\u0441\u043d\u043e\u0435"}

def getMark(args):
	mark = int(args[0])
	return static_mark[mark]

def getDay(args):
	d = args[0]
	if(d > 0):
		return str(d)
	elif(d == 0):
		return u"\u0421\u0435\u0433\u043e\u0434\u043d\u044f"
	else:
		return str(abs(d)) + u" (\u0434\u043e\u043b\u0433)"