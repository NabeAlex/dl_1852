from bookQuery import *
from userQuery import *
from libQuery import *

from messageQuery import *
from contentQuery import *

def get(s, args = []):
	if(s == "books"):
		return getBooks(args)
	elif(s == "count_books"):
		return getCountBooks()
	elif(s == "isset"):
		return isSet(args)
	elif(s == "get_pages"):
		return getPages()
	elif(s == "mark"):
		return getMark(args)
	elif(s == "get_day"):
		return getDay(args)

	if(s == "get_my_books"):
		return getMyBooks()
	elif(s == "normalize_status"):
		return normalizeStatus(args)
	elif(s == "get_menu"):
		return getMenu(args)
	elif (s == "get_my_teacher"):
		return getMyTeacher()
	elif(s == "get_my_pupils"):
		return getMyPupils()
	elif(s == "get_query"):
		return getQuery();

	if(s == "get_news"):
		return getNews()

	if(s == "get_answer_message"):
		return typeAnswer(args)

	if(s == "get_all_pupils"):
		return getAllPupils()
	elif(s == "get_now_class"):
		return getNowClass()
	elif(s == "get_now_number"):
		return getNowNumber()
	elif(s == "get_message"):
		return getMessage()
	elif(s == "get_debtors"):
		return getDebtors()
	elif(s == "get_reservations"):
		return getReservations()