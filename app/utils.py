import datetime

def isInt(el):
	try:
		el = int(el)
	except Exception, e:
		return False
	
	return True

def lastDay(days):
	today = datetime.date.today()
	last = datetime.timedelta(days = days)
	return today - last

def deltaDays(book_passed, need_days):
	last = lastDay(need_days)
	return (book_passed.date() - last).days

def getTime():
	return datetime.datetime.now()

def genScan(id_, with_ = True, count = 4):
	uid = str(id_)
	n = count - len(uid)
	return ("1852" if(with_) else "") + ("0" * n) + uid