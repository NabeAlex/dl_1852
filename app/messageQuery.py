from flask import request

hashMessage = {
	"": 0,
	"ok": 1,
	"no_auth": 2,
	"no_topic": 3,
	"no_message": 4	
}

def typeAnswer(args):
	e = request.args.get("e", "")
	print e
	if(e in hashMessage):
		return hashMessage[e]
	else:
		return 0