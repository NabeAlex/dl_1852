from flask.ext.wtf import Form

from wtforms import TextField, BooleanField, SelectField, DateField, StringField
from wtforms.widgets import TextArea

from wtforms.validators import Required, Length

class LoginForm(Form):
    name = TextField('name')
    last_name = TextField('last_name')


    role = TextField('my_role')
    
    date_Y = TextField('Y')
    date_M = TextField('M')
    date_D = TextField('D')

    classes = [("", "")]
    for i in range(1, 12):
    	classes.append((i, i))

    select_class = SelectField('class', default = 0, choices = classes)

    number_class = SelectField('number', default = 0, choices = [("", ""), (u'\u0410', u'\u0410'), (u'\u0411', u'\u0411'), (u'\u0412', u'\u0412'), \
    																				     (u'\u0413', u'\u0413'), (u'\u0414', u'\u0414')])

    email = TextField('email')
    phone = TextField('phone')

class NewsForm(Form):
    title = TextField('title')
    text = StringField('text', widget=TextArea())
    